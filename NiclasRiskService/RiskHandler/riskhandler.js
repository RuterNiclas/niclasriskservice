//const { jsonp } = require("express/lib/response");

var riskhandlerTestData = require("../RiskHandlerTest/riskhandlertestdata.js");
var ipCalculator = require("./ipCalculator.js");

const WorkerPool = require('./parserWorkerPool')
const path = require('path');
const hostOperatingSystem = require('os')
const Koa = require('koa');

const pool = new WorkerPool(hostOperatingSystem.cpus().length, 
                            path.resolve(__dirname, 'parserWorker.js'));



                                             
 let loginInformationList = []; // In a real example this would be a DATABASE

 loginInformationList = riskhandlerTestData.generateTestData();

 const app = new Koa()

 //http://localhost:3001/?databaseLogId=324234234234
 app.use(async serverContext => {
    const { databaseLogId } = serverContext.query
    serverContext.body = await new Promise((resolve, reject) => {

      pool.runTask({ databaseLogId }, 
                   (err, result) => {
        if (err) {
            serverContext.status = 202;
            return reject(err);
        } else {
            serverContext.status = 202;
            return resolve(result);
        }
      })
    })
});

app.listen(3001)

/*
  JSON object model.
 {
    "UserName": "niclas",
    "IsClientKnown": true,
    "IsIPKnown": true,
    "IsIPInternal": true,
    "IpNumber: "4444.123.001.255",
    "IsLoginSuccessfull": true
    "TimeStamp": 1585526400000  
  }
*/

module.exports = {
    getLoginInformationList: function() {
        return loginInformationList;
    },
    addRawLoginLog: function(logText) {
        handleRawLoginLog(logText);
        return;
    },
    addLoginInformation: function(json) {
        return addLoginInformation(json);
    },
    isUserKnown: function(userName) {
        return isUserKnown(userName);
    },
    isIpKnown: function(ip) {
        return isIpKnown(ip);
    },
    isIpInternal: function(ip) {
        const hardCodedIpSubNetMask = "10.97.2.0/24";
        return ipCalculator.isIpInternal(ip, hardCodedIpSubNetMask);
    },
    getNumberOfFailedLogins: function(userName) {
       return getNumberOfFailedLogins(userName);
    },
    userRiskOverallAssessment: function(userName) {
        return calculateUserRiskOverallAssessment(userName);
    }
};

function handleRawLoginLog(logText) {
    // For this method we put the log to the database in a syncronized manner. 
    // This to never lose the data itself without reflecting this to the API-consumer.
    // Ideally there would be a transaction scoope involved here.
    const primaryKeyGiven = puschRawLogToDatabase(logText);

    // Launch the task to massage the log data here. 
    pool.runTask({databaseLogId: primaryKeyGiven }, 
    (err, result) => {
        if (err) {
            console.log(err);
        } else {
            console.log(result);
        }
    });

    return;
}

function puschRawLogToDatabase(text) {
    // Here the data is pushed to the database
    return "5555555555";  // log primary key generated in database or file system database.
}

function addLoginInformation(json) {
    try {
        const loginInformation = {
            UserName: json.UserName,
            IsClientKnown: json.IsClientKnown,
            IsIPKnown: json.IsIPKnown,
            IsIPInternal: json.IsIPInternal,
            IsLoginSuccessfull: json.IsLoginSuccessfull,
            TimeStamp: json.TimeStamp
          };

          loginInformationList.push(loginInformation);

          return null;
    } catch (error) {
        console.error(error);
        return 'login information was in faulty format.';
    }
}

function isUserKnown(userName) {
    const foundUser = loginInformationList.find(element => element.UserName === userName);

    if(foundUser != null && foundUser !== undefined) {
        return true;
    } else {
        return false;
    }
}

function isIpKnown(ip) {
    const isIpKnown = loginInformationList.find(element => element.IpNumber === ip &&
                                                           element.IsIPKnown === true);
    if(isIpKnown != null && isIpKnown !== undefined) {
        return true;
    } else {
        return false;
    }
}

function getNumberOfFailedLogins(userName) {
    const failedLogins = loginInformationList.filter(element => element.UserName === userName &&
                                                              element.IsLoginSuccessfull === false);

    return failedLogins.length;
}

function calculateUserRiskOverallAssessment(userName) {
    const failedLogins = loginInformationList.filter(element => element.UserName === userName);

    let badnessPoints = 0;

    for (let i = 0; i < failedLogins.length; i++) {
        const failedLogin = failedLogins[i];

        if(failedLogin.IsClientKnown === false) {
            badnessPoints = badnessPoints + 1;
        }
        if(failedLogin.IsIPKnown === false) {
            badnessPoints = badnessPoints + 1;
        }
        if(failedLogin.IsClientKnown === false &&
           failedLogin.IsIPKnown === false) {
            badnessPoints = badnessPoints + 5;
        }
    }

    const minTimeStamp = getLoginListMinTimeStamp(failedLogins);
    const maxTimeStamp = getLoginListMaxTimeStamp(failedLogins);
    const hoursBetweenTimeStamps = (maxTimeStamp - minTimeStamp)  / (1000 * 60 * 60);  
    const forgivenessPerHour = 0.01;
    const forgivenessPoints = hoursBetweenTimeStamps * forgivenessPerHour;

    return badnessPoints - forgivenessPoints;

}

function getLoginListMinTimeStamp(arrayOfLoginInfo) {
   let minValue = Number.POSITIVE_INFINITY;

   for(let i = 0; i < arrayOfLoginInfo.length; i++) {
       if(arrayOfLoginInfo[i].TimeStamp < minValue) {
           minValue = arrayOfLoginInfo[i].TimeStamp;
       }
   }

   return minValue;
}

function getLoginListMaxTimeStamp(arrayOfLoginInfo) {
    let maxValue = Number.NEGATIVE_INFINITY;

    for(let i = 0; i < arrayOfLoginInfo.length; i++) {
        if(arrayOfLoginInfo[i].TimeStamp > maxValue) {
            maxValue = arrayOfLoginInfo[i].TimeStamp;
        }
    }

    return maxValue;
}