
module.exports = {
    isIpInternal: function(ip, hardCodedIpSubNetMask) {
        return isIpInternal(ip, hardCodedIpSubNetMask);
    }
};


// https://www.w3schools.com/js/js_operators.asp
// &	AND	
// |	OR	
// ~	NOT	
// ^	XOR	
// <<	left shift	
// >>	right shift	
// >>>	unsigned right shift
// +variable interprets the variable as number
// Here we assume IPv4
function isIpInternal(ip, ipSubnetMask) {
    let stringSubnetMask = `${ipSubnetMask}`;
    let subnetMaskParts = splitByMany(['.','/'], stringSubnetMask);
    // Had to google this!
    let flatternSubmaskTo32Bits = subnetMaskParts.slice(0, 4).reduce(function (a, o) {
                     return unsignedRightShiftToZero(+a << 8) + +o;
                 });

    // Seems to be no other way of finding int.Max than not zero.
    let mask = unsignedRightShiftToZero(~0 << (32 - +subnetMaskParts[4])); // 0xffffff00
    let unassignedStart = unsignedRightShiftToZero(flatternSubmaskTo32Bits & mask);
    let startIpPosition = buildIpParts(unassignedStart); 
    let unassignedEnd = unsignedRightShiftToZero(flatternSubmaskTo32Bits | ~mask);
    let endIpPosition = buildIpParts(unassignedEnd);

    let testIpSplit = ip.split('.');
    if(startIpPosition[0] <= +testIpSplit[0] && 
       startIpPosition[1] <= +testIpSplit[1] &&
       startIpPosition[2] <= +testIpSplit[2] &&
       startIpPosition[3] <= +testIpSplit[3] &&
       endIpPosition[0] >= testIpSplit[0] &&
       endIpPosition[1] >= testIpSplit[1] &&
       endIpPosition[2] >= testIpSplit[2] &&
       endIpPosition[3] >= testIpSplit[3]) {
        return true;
    }

    return false;

}

// It MUST be a better method than this!
function splitByMany( manyArgs, string ) {
    do {
      let arg = manyArgs.pop()
      string = string.replace(arg, manyArgs[0])
    } while (manyArgs.length > 2)
    return string.split(manyArgs[0])
  }

function unsignedRightShiftToZero(n) { 
    return n >>> 0; 
}

// Pack the number in to octates.
function buildIpParts(n) {
    let result = [
        (n >>> 24) & 0xFF,
        (n >>> 16) & 0xFF,
        (n >>>  8) & 0xFF,
        (n >>>  0) & 0xFF
    ];

    return result;
}