// const { start } = require('repl');
const { parentPort, threadId } = require('worker_threads');

var riskhandlerTestData = require("../RiskHandlerTest/riskhandlertestdata.js");

parentPort.on("message", (task) => {
    const { databaseLogId } = task
    console.log("Running parse raw log with id: " + databaseLogId + " on thread: " + threadId )
    parentPort.postMessage(parseData(databaseLogId))
})

function parseData(databaseLogId) {
   
    var logRawText = queryDatabaseOfRawLog(databaseLogId);
    var parsedResult = parseRawLoginLog(logRawText);
    storeParsedResultToDatabase(parsedResult);

    markRawLogAsParsed(databaseLogId);
    sleep(20000);

    return "System successfully parsed raw log from database to structured log: " + databaseLogId;
}

function sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

function queryDatabaseOfRawLog(databaseLogId) {
    return "Raw log text";
}

function markRawLogAsParsed(databaseLogId) {
    // Marks the log as parsed in the database.
}

function storeParsedResultToDatabase(parsedResult) {
  // Store
}

function parseRawLoginLog(logText) {
    const userName = riskhandlerTestData.makeUserName(10);
    const IsClientKnown = riskhandlerTestData.makeRandomBool();
    const IsIPKnown = riskhandlerTestData.makeRandomBool();
    const IsIPInternal = riskhandlerTestData.makeRandomBool();
    const IsLoginSuccessfull = riskhandlerTestData.makeRandomBool();
    const IpNumber = riskhandlerTestData.makeIpNumber();
    const TimeStamp = Date.now();

    const loginInformation = {
        UserName: userName,
        IsClientKnown: IsClientKnown,
        IsIPKnown: IsIPKnown,
        IsIPInternal: IsIPInternal,
        IsLoginSuccessfull: IsLoginSuccessfull,
        IpNumber: IpNumber,
        TimeStamp: TimeStamp
      };

    return loginInformation;
}

