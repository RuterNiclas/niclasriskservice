var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var riskhandler = require("./RiskHandler/riskhandler.js");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

// API start up

const logSubDomainName = 'log'
const riskSubDomainName = 'risk';

// GET http://localhost:3000/ping
app.get('/ping', function(req, res) {
  res.status(200).json({ message: 'Hello world' });
});
// PUT http://localhost:3000/log/register-raw-log
app.put('/' + logSubDomainName +  '/register-raw-log', function (req, res) {
  
  var body = req.body;

  if(isEmptyAPIParameter(body.Data)) {
    res.status(400).json({ result: 'Request body data must be entered in key: Data' });
  }
  else {
    var value = riskhandler.addRawLoginLog(body.Data);
    res.status(202).json({ result: 'success' });
  }
}); 

/*
  JSON object model.
   {
    "UserName": "niclas",
    "IsClientKnown": true,
    "IsIPKnown": true,
    "IsIPInternal": true,
    "IsLoginSuccessfull": true,
    "IpNumber": "4444.123.001.255",
    "TimeStamp": 1585526400000  
  }

  Postman requires explicit setting of JSON payload.
*/
// PUT http://localhost:3000/log/register-login-information
app.put('/' + logSubDomainName +  '/register-login-information', function (req, res) {
  var body = req.body;
  //const json = JSON.parse(body);

  const executionResult = riskhandler.addLoginInformation(body);

  if (executionResult != null && executionResult !== undefined) {
    res.status(400).json({ result: executionResult });
  } else {
    res.status(202).json({ result: 'success' });
  }
});

// Do gets straight out of browser

// GET http://localhost:3000/risk/is-user-known/Niclas
app.get('/' + riskSubDomainName + '/is-user-known/:userName', function (req, res) {

  const userName = req.params.userName;
  // Could deal with bad parameter input
  const foundUser = riskhandler.isUserKnown(userName);

  res.status(200).json({ result: foundUser });
 
});

// GET http://localhost:3000/risk/is-ip-known/ip
app.get('/' + riskSubDomainName + '/is-ip-known/:ip', function (req, res) {
  const ip = req.params.ip;
// Could deal with bad parameter input
  const foundUser = riskhandler.isIpKnown(ip);

  res.status(200).json({ result: foundUser });

});

// GET http://localhost:3000/risk/is-ip-internal/ip
app.get('/' + riskSubDomainName + '/is-ip-internal/:ip', function (req, res) {
  const ip = req.params.ip;
// Could deal with bad parameter input
  const foundUser = riskhandler.isIpInternal(ip);

  res.status(200).json({ result: foundUser });

});

//GET http://localhost:3000/risk/get-number-of-failed-logins/userName
app.get('/' + riskSubDomainName + '/get-number-of-failed-logins/:userName', function (req, res) {
  const userName = req.params.userName;
// Could deal with bad parameter input
  const numberOfFailedLogins = riskhandler.getNumberOfFailedLogins(userName);

  res.status(200).json({ result: numberOfFailedLogins });

});

// GET http://localhost:3000/risk/user-risk-overall-assessment/userName
app.get('/' + riskSubDomainName + '/user-risk-overall-assessment/:userName', function (req, res) {
  const userName = req.params.userName;
  // Could deal with bad parameter input
  const riskNumber = riskhandler.userRiskOverallAssessment(userName);

  res.status(200).send({ result: riskNumber});

});

function isEmptyAPIParameter(str) {
  return (!str || str.length === 0 );
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
