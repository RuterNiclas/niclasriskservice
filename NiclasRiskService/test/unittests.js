var assert = require('assert');

describe('Riskhandler', function() {

  var riskhandler = require("../RiskHandler/riskhandler.js");

  describe('#handleRawLoginLog', function() {

    const testInfo1 = "1234567890";

    try
    {
      riskhandler.addRawLoginLog(testInfo1);
    }
    catch(error){
      assert(error, 'addRawLoginLog: failed');
    }
  });

  describe('#addLoginInformation', function() {

    const testInfo2 = {
      UserName: "ABC123",
      IsClientKnown: true,
      IsIPKnown: true,
      IsIPInternal: true,
      IpNumber: "333.123.001.555",
      IsLoginSuccessfull: true,
      TimeStamp: new Date('2021-01-01 05:00:00').getTime()
    };

    const result = riskhandler.addLoginInformation(testInfo2);
    
    const registeredEntries = riskhandler.getLoginInformationList().filter(x => x.UserName === "ABC123");
    const numberOfEntries = registeredEntries.length;

    it('should return null or undefined when the operation is successfull', function() {
      assert.equal(result == null || result === undefined, true);
    });
    
    it('should return have stored a single object when the operation is successfull', function() {
      
      assert.equal(numberOfEntries, 1);
    });
  });

  describe('#isUserKnown', function() {
    const userName = "nicsmeadmin";
    const result = riskhandler.isUserKnown(userName);
    it('should return a json true', function() {
      
      assert.equal(result, true);
    });

  });

  describe('#isIpKnown', function() {
    const ip1 = "111.111.111.111";
    const result1 = riskhandler.isIpKnown(ip1);
    const ip2 = "333.123.001.555";
    const result2 = riskhandler.isIpKnown(ip2);
    it('IP 1 should return a json true', function() {
      
      assert.equal(result1, false);
    });
    it('IP 2 should return a json false', function() {
      
      assert.equal(result2, true);
    });

  });

  describe('#getNumberOfFailedLogins', function() {
    const userName = "badHacker";
    const result = riskhandler.getNumberOfFailedLogins(userName);
    it('Bad hacker failed to log in correct number of times', function() {
      
      assert.equal(result, 5);
    });
  });  

  describe('#userRiskOverallAssessment', function() {
    const userName = "nicsmeadmin";
    const result = riskhandler.userRiskOverallAssessment(userName);
    it('Over all assessment value should be correct calculated', function() {
      
      assert.equal(result, 13.88);
    });
  });  
});


describe('ipCalculator', function() {

  var ipCalculator = require("../RiskHandler/ipCalculator.js");
  var networkMask1 = "10.97.2.0/24"; // CIDR
  var networkMask2 = "10.97.2.0/20"; // CIDR

  describe('#Mask1IpLower', function() {
    const result = ipCalculator.isIpInternal("10.97.1.1", networkMask1)

    it('should return a json true', function() {
      assert.equal(result, false);
    });
  });

  // I don't understand why this turns out true!
  // Broadcast reserved?
  describe('#Mask1IpHigher1', function() {
    const result = ipCalculator.isIpInternal("10.97.2.255", networkMask1)

    it('should return a json true', function() {
      assert.equal(result, false);
    });
  });

   // I don't understand why this turns out true!
  describe('#Mask1IpHigher2', function() {
    const result = ipCalculator.isIpInternal("10.97.3.128", networkMask1)
  
    it('should return a json true', function() {
      assert.equal(result, false);
    });
  });

  describe('#Mask1IpInRange', function() {
    const result = ipCalculator.isIpInternal("10.97.2.128", networkMask1)

    it('should return a json true', function() {
      assert.equal(result, true);
    });
  });

  describe('#Mask2IpLower', function() {
    const result = ipCalculator.isIpInternal("10.96.255.1", networkMask2)

    it('should return a json true', function() {
      assert.equal(result, false);
    });
  });

  // I don't understand why this turns out true!
  describe('#Mask2IpHigher1', function() {
    const result = ipCalculator.isIpInternal("10.97.16.1", networkMask2)

    it('should return a json true', function() {
      assert.equal(result, false);
    });
  });

  describe('#Mask2IpInRange', function() {
    const result = ipCalculator.isIpInternal("10.97.15.100", networkMask2)

    it('should return a json true', function() {
      assert.equal(result, true);
    });
  });

});


