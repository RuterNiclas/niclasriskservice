const res = require("express/lib/response");
const { jsonp } = require("express/lib/response");

module.exports = {    
    generateTestData: function() {
       return generateTestData();
    },
    makeUserName: function(length) {
       return makeRandomText(length, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');
    },
    makeIpNumber: function() {
     return makeRandomIP();
    },
    makeRandomBool: function() {
      return makeRandomBool();
    }
};

function generateTestData() {
  let loginInformationList = [];

  const niclas1 = {
      UserName: "nicsmeadmin",
      IsClientKnown: true,
      IsIPKnown: true,
      IsIPInternal: true,
      IpNumber: "333.123.001.555",
      IsLoginSuccessfull: true,
      TimeStamp: new Date('2021-01-01 05:00:00').getTime()
    };
    const niclas2 = {
      UserName: "nicsmeadmin",
      IsClientKnown: false,
      IsIPKnown: false,
      IsIPInternal: false,
      IpNumber: "333.123.001.111",
      IsLoginSuccessfull: true,
      TimeStamp: new Date('2021-01-01 10:00:00').getTime()
    };
    const niclas3 = {
      UserName: "nicsmeadmin",
      IsClientKnown: false,
      IsIPKnown: false,
      IsIPInternal: false,
      IpNumber: "333.123.001.112",
      IsLoginSuccessfull: false,
      TimeStamp: new Date('2021-01-01 10:30:00').getTime()
    };
    const niclas4 = {
      UserName: "nicsmeadmin",
      IsClientKnown: true,
      IsIPKnown: true,
      IsIPInternal: true,
      IpNumber: "333.123.001.113",
      IsLoginSuccessfull: true,
      TimeStamp: new Date('2021-01-01 17:00:00').getTime()
    };

    loginInformationList.push(niclas1);
    loginInformationList.push(niclas2);
    loginInformationList.push(niclas3);
    loginInformationList.push(niclas4);

    const badHacker1 = {
      UserName: "badHacker",
      IsClientKnown: false,
      IsIPKnown: true,
      IsIPInternal: false,
      IpNumber: "4444.123.001.113",
      IsLoginSuccessfull: false,
      TimeStamp: new Date('2021-01-01 17:00:00').getTime()
    };
    const badHacker2 = {
      UserName: "badHacker",
      IsClientKnown: false,
      IsIPKnown: false,
      IsIPInternal: false,
      IpNumber: "4444.123.001.222",
      IsLoginSuccessfull: false,
      TimeStamp: new Date('2021-01-01 17:00:01').getTime()
    };
    const badHacker3 = {
      UserName: "badHacker",
      IsClientKnown: true,
      IsIPKnown: false,
      IsIPInternal: false,
      IpNumber: "4444.123.001.224",
      IsLoginSuccessfull: false,
      TimeStamp: new Date('2021-01-01 17:00:02').getTime()
    };
    const badHacker4 = {
      UserName: "badHacker",
      IsClientKnown: false,
      IsIPKnown: false,
      IsIPInternal: false,
      IpNumber: "4444.123.001.227",
      IsLoginSuccessfull: false,
      TimeStamp: new Date('2021-01-01 17:00:03').getTime()
    };
    const badHacker5 = {
      UserName: "badHacker",
      IsClientKnown: false,
      IsIPKnown: false,
      IsIPInternal: false,
      IpNumber: "4444.123.001.255",
      IsLoginSuccessfull: false,
      TimeStamp: new Date('2021-01-01 17:00:04').getTime()
    };
    const badHacker6 = {
      UserName: "badHacker",
      IsClientKnown: false,
      IsIPKnown: true,
      IsIPInternal: false,
      IpNumber: "4444.123.001.543",
      IsLoginSuccessfull: true,
      TimeStamp: new Date('2021-01-01 17:00:05').getTime()
    };

    loginInformationList.push(badHacker1);
    loginInformationList.push(badHacker2);
    loginInformationList.push(badHacker3);
    loginInformationList.push(badHacker4);
    loginInformationList.push(badHacker5);
    loginInformationList.push(badHacker6);

    return loginInformationList;
}

function makeRandomText(length,characters ) {
  var result           = '';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
 return result;
}

function makeRandomIP() {
  let result = makeRandomText(3, '0123456789') + '.';
  result = result + makeRandomText(3, '0123456789') + '.'
  result = result + makeRandomText(3, '0123456789') + '.'
  result = result + makeRandomText(3, '0123456789');

 return result;
}

function makeRandomBool() {
  const result = Math.random();
  return result > 0.5;
}
